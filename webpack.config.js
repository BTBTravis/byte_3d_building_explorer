module.exports = [
    {
        name: "index",
        entry: "./src/index.js",
        output: {
            path: __dirname + "/build",
            filename: "building_explorer.js"
        }
    }
];
